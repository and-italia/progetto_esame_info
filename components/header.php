<?php
  session_start();

  include_once 'components/cookies.php';
  include_once 'components/permissions.php';
  fill_session_from_cookie_if_exists();

  $login_btn = "Log In/Registrati";
  $saluto_utente = "";
  if (isset($_SESSION["user"]["logged"]) && $_SESSION["user"]["logged"]) {
    $login_btn = "Log Out";
    $saluto_utente = "Ciao " . $_SESSION["user"]["data"]["nome"] . " " . $_SESSION["user"]["data"]["cognome"] . ", è bello rivederti!";
  }
?>

<!DOCTYPE html>
<html lang="zxx">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <?php include_once 'components/player.php'; ?>
    <title>La radio universitaria</title>
  </head>
  <body>
    <!-- Navbar -->
    <header>
      <nav class = "navigation_bar">
        <a href="#" class = "logo_nav_box"><img class = "logo_nav" src="assets/logo.png" alt="Logo radio"></a>
        <div class = "navigation_elements">
          <div class="navigation_elements_items">
            <a class = "nav_item" href="index.php"><i class="fas fa-home"></i>Scopri</a>
            <a class = "nav_item" href="naviga.php"><i class="fas fa-th-list"></i>Naviga</a>
            <a class = "nav_item" href="palinsesto.php"><i class="fas fa-broadcast-tower"></i>Palinsesto</a>
            <a class = "nav_item" href="partners.php"><i class="far fa-handshake"></i>Partners</a>
            <?php if (isset($_SESSION["user"]["logged"]) && $_SESSION["user"]["logged"]){ ?>
              <a class = "nav_item" href="settings.php"><i class="fas fa-address-card"></i>Impostazioni</a>
            <?php } ?>
            <a class = "nav_item login current_page" href="login.php"><i class="fas fa-sign-in-alt"></i><?=$login_btn ?></a>
          </div>
        </div>
      </nav>
    </header>

    <h2 class = "saluto_utente"><?=$saluto_utente ?></h2>
