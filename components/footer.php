<footer>
  <p>
    <strong>© 2018 Simone Martinelli e Lorenzo Luigi Forina</strong>
    <br>
    Progetto d'esame "Radio 110 - UniTo"
    <br>
    <br>
    <a href="http://www.unito.it" title="Università degli Studi di Torino">unito.it</a><span class="blue"> |</span><a href="http://www.unito.it/profilo/press" title="Staff Comunicazione">Staff Comunicazione e Relazioni esterne</a><span class="blue"> |</span><a href="http://www.unito.it/privacy" title="Privacy">Privacy</a><span class="blue"> |</span><a href="http://www.unito.it/note-legali" title="Note legali">Note legali</a>
  </p>
</footer>
</body>
</html>
