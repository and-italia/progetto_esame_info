<?php
  include_once 'model/model.php';
  include_once 'model/contenuti.php';

  if (isset($_GET["idPlaying"])) {
    $contenuto = get_contenuti_by_id($_GET["idPlaying"]);
    $autore_player = $contenuto["autore"];
    $titolo_player = $contenuto["titolo"];
    $source = $contenuto["filename"];
  } else {
    $autore_player = "Scegli una canzone";
    $titolo_player = "-";
    $source = "";
  }
?>

<div class="audio_player shadow">
  <!-- <aside id = "listen_live_radio_box">
    <img id = "listen_live_radio" src="assets/listen_live_radio.png" alt="Ascolta la web radio" title="Ascolta la web radio110">
  </aside> -->
  <div>
    <h3><?=$autore_player ?></h3>
    <p>
      <?=$titolo_player ?>
    </p>
  </div>
  <audio src="<?=$source ?>" controls autoplay>
  </audio>
</div>
