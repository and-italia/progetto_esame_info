<?php

function require_login()
{
  if (!isset($_SESSION["user"]["logged"]) || !$_SESSION["user"]["logged"]) {
    header("Location: login.php");
    die();
  }
}

function require_admin()
{
  if (!isset($_SESSION["user"]["logged"]) || !$_SESSION["user"]["data"]["user_type_key"] == "admin") {
    header("Location: login.php");
    die();
  }
}

?>
