<?php
  function _set_cookie($data)
  {
    setcookie("ESAME_INFO_", serialize($data), time() + (86400 * 30), "/");
  }

  function fill_session_from_cookie_if_exists()
  {
    if (isset($_COOKIE["ESAME_INFO_"]) && $_COOKIE["ESAME_INFO_"]) {
      $_SESSION["user"] = unserialize($_COOKIE["ESAME_INFO_"], ["allowed_classes" => false]);
    }
  }

  function delete_cookie()
  {
    setcookie("ESAME_INFO_", serialize($data), time() - (86400 * 30), "/");
  }
?>
