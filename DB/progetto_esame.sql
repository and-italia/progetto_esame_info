-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Giu 03, 2019 alle 12:18
-- Versione del server: 10.1.32-MariaDB
-- Versione PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `progetto_esame`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `articoli`
--

CREATE TABLE `articoli` (
  `id_articolo` int(11) NOT NULL,
  `titolo` varchar(256) NOT NULL,
  `testo` text NOT NULL,
  `key_genere` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `contenuti`
--

CREATE TABLE `contenuti` (
  `id_contenuto` int(11) NOT NULL,
  `key_genere` varchar(64) NOT NULL,
  `titolo` varchar(128) NOT NULL,
  `autore` varchar(128) NOT NULL,
  `playlist` varchar(128) NOT NULL,
  `album` varchar(128) NOT NULL,
  `filename` varchar(128) NOT NULL,
  `copertina` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `contenuti`
--

INSERT INTO `contenuti` (`id_contenuto`, `key_genere`, `titolo`, `autore`, `playlist`, `album`, `filename`, `copertina`) VALUES
(1, 'indie', 'Paracetamolo', 'Calcutta', 'Musica da viaggio', '', 'assets/music/paracetamolo.mp3', 'assets/posts/cover_1.jpg'),
(2, 'indie', 'Verdura', 'Pinguini tattici nucleari', '', '', 'assets/music/verdura.mp3', 'assets/posts/cover_4.jpg'),
(5, 'rock', 'All the small things', 'Blink 182', 'Musica da viaggio', '', 'assets/music/all_the_small_thngs.mp3', 'assets/posts/cover_3.jpg'),
(6, 'rock', 'American idiot', 'Green day', '', 'American idiot', 'assets/music/american_idiot.mp3', 'assets/posts/cover_2.png'),
(7, 'indie', 'Kiwi', 'Calcutta', 'Musica da viaggio', '', 'assets/music/kiwi.mp3', 'assets/posts/cover_5.png'),
(9, 'rock', 'Afterlife', 'Avenged Sevenfold', '', 'Avenged Sevenfold', 'assets/music/avenged.mp3', 'assets/posts/cover_6.png'),
(10, 'rock', 'Still waiting', 'Sum 41', '', 'Does this look infected?', 'assets/music/still_waiting.mp3', 'assets/posts/cover_7.jpg'),
(11, 'audiolibri', 'I promessi sposi', 'Alessandro Manzoni', '', '', 'assets/music/promessi_sposi.mp3', 'assets/posts/promessi_sposi.jpg'),
(12, 'podcast', 'London vibes', 'Screwpolus', 'London Vibes', '', 'assets/music/london_vibes.mp3', 'assets/posts/london_vibes.jpg'),
(18, 'rock', 'Boulevard of broken dreams', 'Green Day', '', 'American Idiot', 'assets/music/boulevard of broken dreams.mp3', 'assets/posts/cover_2.png'),
(19, 'rap', 'LunedÃ¬', 'Salmo', 'Musica da viaggio', 'Playlist', 'assets/music/lunedÃ¬ - salmo.mp3', 'assets/posts/salmo-lunedÃ¬.jpg');

-- --------------------------------------------------------

--
-- Struttura della tabella `generi`
--

CREATE TABLE `generi` (
  `key_generi` varchar(64) NOT NULL,
  `descrizione` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `generi`
--

INSERT INTO `generi` (`key_generi`, `descrizione`) VALUES
('audiolibri', 'Audiolibri'),
('indie', 'Musica Indie'),
('podcast', 'Podcast'),
('rap', 'Musica rap'),
('rock', 'Genere rock');

-- --------------------------------------------------------

--
-- Struttura della tabella `preferenze`
--

CREATE TABLE `preferenze` (
  `id_preferenza` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `key_genere` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `preferenze`
--

INSERT INTO `preferenze` (`id_preferenza`, `username`, `key_genere`) VALUES
(9, 'Simone', 'rock');

-- --------------------------------------------------------

--
-- Struttura della tabella `user_type`
--

CREATE TABLE `user_type` (
  `user_type_key` varchar(64) NOT NULL,
  `descrizione` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `user_type`
--

INSERT INTO `user_type` (`user_type_key`, `descrizione`) VALUES
('admin', 'Utente amministratore'),
('user', 'Utente normale');

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `username` varchar(64) NOT NULL,
  `user_type_key` varchar(64) NOT NULL,
  `nome` varchar(128) NOT NULL,
  `cognome` varchar(128) NOT NULL,
  `mail` varchar(128) NOT NULL,
  `password` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`username`, `user_type_key`, `nome`, `cognome`, `mail`, `password`) VALUES
('admin', 'admin', 'Gianni', 'Rossi', 'g.rossi@and-italia.it', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Simone', 'admin', 'Simone', 'Martinelli', 's.martinelli@and-italia.it', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'),
('user', 'user', 'Franco', 'Verdi', 'f.verdi@and-italia.it', '04f8996da763b7a969b1028ee3007569eaf3a635486ddab211d512c85b9df8fb');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `articoli`
--
ALTER TABLE `articoli`
  ADD PRIMARY KEY (`id_articolo`),
  ADD KEY `articoli_genere` (`key_genere`);

--
-- Indici per le tabelle `contenuti`
--
ALTER TABLE `contenuti`
  ADD PRIMARY KEY (`id_contenuto`),
  ADD KEY `contenuti_generi` (`key_genere`);

--
-- Indici per le tabelle `generi`
--
ALTER TABLE `generi`
  ADD PRIMARY KEY (`key_generi`);

--
-- Indici per le tabelle `preferenze`
--
ALTER TABLE `preferenze`
  ADD PRIMARY KEY (`id_preferenza`),
  ADD KEY `preferenze_generi` (`key_genere`),
  ADD KEY `preferenze_utenti` (`username`);

--
-- Indici per le tabelle `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`user_type_key`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`username`),
  ADD KEY `user_with_usertype` (`user_type_key`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `articoli`
--
ALTER TABLE `articoli`
  MODIFY `id_articolo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `contenuti`
--
ALTER TABLE `contenuti`
  MODIFY `id_contenuto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT per la tabella `preferenze`
--
ALTER TABLE `preferenze`
  MODIFY `id_preferenza` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `articoli`
--
ALTER TABLE `articoli`
  ADD CONSTRAINT `articoli_genere` FOREIGN KEY (`key_genere`) REFERENCES `generi` (`key_generi`);

--
-- Limiti per la tabella `contenuti`
--
ALTER TABLE `contenuti`
  ADD CONSTRAINT `contenuti_generi` FOREIGN KEY (`key_genere`) REFERENCES `generi` (`key_generi`);

--
-- Limiti per la tabella `preferenze`
--
ALTER TABLE `preferenze`
  ADD CONSTRAINT `preferenze_generi` FOREIGN KEY (`key_genere`) REFERENCES `generi` (`key_generi`),
  ADD CONSTRAINT `preferenze_utenti` FOREIGN KEY (`username`) REFERENCES `utenti` (`username`);

--
-- Limiti per la tabella `utenti`
--
ALTER TABLE `utenti`
  ADD CONSTRAINT `user_with_usertype` FOREIGN KEY (`user_type_key`) REFERENCES `user_type` (`user_type_key`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
