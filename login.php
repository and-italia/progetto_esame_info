<?php
  include_once 'components/header.php';

  include_once 'model/model.php';
  include_once 'model/utenti.php';
  include_once 'model/preferenze.php';

  $error_message = "";

  if (isset($_SESSION["user"]["logged"]) && $_SESSION["user"]["logged"]) {
    session_destroy();
    delete_cookie();
    header("Location: index.php");
    die();
  }

  if (isset($_POST["username"]) && isset($_POST["password"])) {
    if ($_POST["username"] && $_POST["password"]) {
      $_SESSION["user"]["logged"] = login($_POST["username"], $_POST["password"]);
      if ($_SESSION["user"]["logged"]) {
        $_SESSION["user"]["data"] = get_user($_POST["username"]);
        $_SESSION["user"]["preference"] = get_preference($_POST["username"]);
        _set_cookie($_SESSION["user"]);
        header("Location: index.php");
        die();
      } else {
        $error_message = "Nome utente o password sono errati";
      }
    } else {
      $error_message = "Devi inserire tutti i campi";
    }
  }
?>
<section class = "container">
  <div class="content">
    <h1>Accedi alla piattaforma radio</h1>
    <section class="form_container">
      <form class="registration_form shadow" action="login.php" method="post">
        <div class = "registration_form_title">
          <h3>Accedi qui</h3>
        </div>
        <input type="text" name="username" placeholder="Username" title="username">
        <br>
        <input type="password" name="password" placeholder="Password" title="password">
        <br>
        <button class = "shadow" type="submit" name="button">Invia</button>
        <label style="color:red"><?=$error_message ?></label>
        <br>
        <a href="registration.php">Non sei registrato? Clicca qui.</a>
      </form>
    </section>
  </div>
</section>
<?php include_once 'components/footer.php'; ?>
