<?php
  include_once 'components/header.php';

  include_once 'model/model.php';
  include_once 'model/utenti.php';
  include_once 'model/preferenze.php';

  $error_message = "";

  if (isset($_SESSION["user"]["logged"]) && $_SESSION["user"]["logged"]) {
    header("Location: index.php");
    die();
  }

  if (isset($_POST["username"]) && isset($_POST["name"]) && isset($_POST["surname"]) && isset($_POST["mail"]) && isset($_POST["password"])) {
    if ($_POST["username"] && $_POST["name"] && $_POST["surname"] && $_POST["mail"] && $_POST["password"]) {
      if (is_username_free($_POST["username"])) {
        insert_user($_POST["username"], $_POST["name"], $_POST["surname"], $_POST["mail"], $_POST["password"]);
        header("Location: registration_ok.php");
        die();
      } else {
        $error_message = "Lo username è già stato utilizzato";
      }
    } else {
      $error_message = "Devi inserire tutti i campi";
    }
  }
?>
<section class = "container">
  <div class="content">
    <section class="form_container">
      <h2 class="centered">Registrati qui</h2>
      <h3 class = "centered">Una vasta offerta di contenuti multimediali a portata di click</h3>
      <form class="registration_form shadow" action="registration.php" method="post">
        <div class = "registration_form_title">
          <label style="color:red"><?=$error_message ?></label>
          <h3>Registrati qui</h3>
        </div>
        <input type="text" name="username" title="username" placeholder="Username">
        <br>
        <input type="text" name="name" title="name" placeholder="Nome">
        <br>
        <input type="text" name="surname" title="surname" placeholder="Cognome">
        <br>
        <input type="mail" name="mail" title="mail" placeholder="Mail">
        <br>
        <input type="password" name="password" title="password" placeholder="Password">
        <br>
        <button class = "shadow" type="submit" name="button">Invia</button>
      </form>
    </section>
  </div>
</section>
<?php include_once 'components/footer.php'; ?>
