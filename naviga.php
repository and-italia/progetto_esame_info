<?php
  include_once 'components/header.php';

  include_once 'model/model.php';
  include_once 'model/contenuti.php';

  if (isset($_POST["string_to_search"])) {
    $contenuti = search_contenuti($_POST["string_to_search"]);
  } else {
    $contenuti = search_contenuti('');
  }

?>
<style media="screen">

</style>

<section class = "container">
  <div class="content">
    <section class="filter-box centered">
      <form class="searchbar"  action="naviga.php" method="post">
        <input class="text_field" type="text" name="string_to_search" value="" placeholder="Cerca tra titoli, autori, generi, playlist e album.">
        <input class="button" type="submit" name="" value="Cerca">
      </form>
    </section>
    <table class="palinsesto_table">
      <tr>
        <th>Titolo</th>
        <th>Autore</th>
        <th>Album</th>
        <th>Playlist</th>
        <th>Genere</th>
        <th>Riproduci</th>
      </tr>
      <?php foreach ($contenuti as $contenuto){ ?>
        <tr>
          <th><?= $contenuto[2]?></th>
          <th><?= $contenuto[3]?></th>
          <th><?= $contenuto[5]?></th>
          <th><?= $contenuto[4]?></th>
          <th><?= $contenuto[1]?></th>
          <th>
            <form action="naviga.php" method="get">
              <input type="hidden" name="idPlaying" value="<?=$contenuto[0] ?>">
              <input class="play_button" type="submit" name="" value="Riproduci">
            </form>
          </th>
        </tr>
      <?php } ?>
    </table>
    <?php echo ($contenuti) ? "<h4 class = 'centered'>Risultati trovati: " . count($contenuti) . "</h4>" : "<h4 class = 'centered'>La ricerca non ha prodotto risultati</h4>" ; ?>
  </section>
</div>
</section>
  </div>
</section>
<?php include_once 'components/footer.php'; ?>
