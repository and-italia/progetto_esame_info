<?php
  include_once 'components/header.php';

  include_once 'model/model.php';
  include_once 'model/contenuti.php';
  include_once 'model/preferenze.php';
?>
  <section class = "container">
    <div class="content">
      <h1>AppRadio - Contenuti</h1>
      <?php if (isset($_SESSION["user"]["logged"]) && $_SESSION["user"]["logged"] && $_SESSION["user"]["preference"]){ ?>
        <h1>Scelti per <?=$_SESSION["user"]["data"]["nome"] ?></h1>
        <section class = "news_box">
          <?php foreach (get_contenuti_by_genere($_SESSION["user"]["preference"]) as $contenuto){ ?>
            <div class = "news_item shadow border_radius">
              <img src="<?=$contenuto[7] ?>" alt="Album numero 1" title="Album numero 1">
              <h3><?=$contenuto[2] ?></h3>
              <h4><?=$contenuto[3] ?></h4>
              <div class ="news_item_description">
                <form action="index.php" method="get">
                  <input type="hidden" name="idPlaying" value="<?=$contenuto[0] ?>">
                  <input class="play_button" type="submit" name="" value="Riproduci">
                </form>
              </div>
            </div>
          <?php } ?>
        </section>
      <?php } ?>

      <h1>Audiolibri</h1>
      <section class = "news_box">
        <?php foreach (get_contenuti_by_genere("audiolibri") as $contenuto){ ?>
          <div class = "news_item shadow border_radius">
            <img src="<?=$contenuto[7] ?>" alt="Album numero 1" title="Album numero 1">
            <h3><?=$contenuto[2] ?></h3>
            <h4><?=$contenuto[3] ?></h4>
            <div class ="news_item_description">
              <form action="index.php" method="get">
                <input type="hidden" name="idPlaying" value="<?=$contenuto[0] ?>">
                <input class="play_button" type="submit" name="" value="Riproduci">
              </form>
            </div>
          </div>
        <?php } ?>
      </section>

      <h1>Musica da viaggio</h1>
      <section class = "news_box">
        <?php foreach (get_contenuti_by_playlist("Musica da viaggio") as $contenuto){ ?>
          <div class = "news_item shadow border_radius">
            <img src="<?=$contenuto[7] ?>" alt="Album numero 1" title="Album numero 1">
            <h3><?=$contenuto[2] ?></h3>
            <h4><?=$contenuto[3] ?></h4>
            <div class ="news_item_description">
              <form action="index.php" method="get">
                <input type="hidden" name="idPlaying" value="<?=$contenuto[0] ?>">
                <input class="play_button" type="submit" name="" value="Riproduci">
              </form>
            </div>
          </div>
        <?php } ?>
      </section>

      <h1>Podcast</h1>
      <section class = "news_box">
        <?php foreach (get_contenuti_by_genere("podcast") as $contenuto){ ?>
          <div class = "news_item shadow border_radius">
            <img src="<?=$contenuto[7] ?>" alt="Album numero 1" title="Album numero 1">
            <h3><?=$contenuto[2] ?></h3>
            <h4><?=$contenuto[3] ?></h4>
            <div class ="news_item_description">
              <form action="index.php" method="get">
                <input type="hidden" name="idPlaying" value="<?=$contenuto[0] ?>">
                <input class="play_button" type="submit" name="" value="Riproduci">
              </form>
            </div>
          </div>
        <?php } ?>
      </section>

      <h1>Tutti i contenuti</h1>
      <section class = "news_box">
        <?php foreach (get_contenuti() as $contenuto){ ?>
          <div class = "news_item shadow border_radius">
            <img src="<?=$contenuto[7] ?>" alt="Album numero 1" title="Album numero 1">
            <h3><?=$contenuto[2] ?></h3>
            <h4><?=$contenuto[3] ?></h4>
            <div class ="news_item_description">
              <form action="index.php" method="get">
                <input type="hidden" name="idPlaying" value="<?=$contenuto[0] ?>">
                <input class="play_button" type="submit" name="" value="Riproduci">
              </form>
            </div>
          </div>
        <?php } ?>
      </section>
    </div>
  </section>
<?php include_once 'components/footer.php'; ?>
