<?php
  include_once 'components/header.php';
  require_login();

  include_once 'model/model.php';
  include_once 'model/generi.php';
  include_once 'model/utenti.php';
  include_once 'model/preferenze.php';

  $err_login_info = "";
  $err_password = "";

  if (isset($_POST["nome"]) && isset($_POST["cognome"]) && isset($_POST["mail"])) {
    if ($_POST["nome"] && $_POST["cognome"] && $_POST["mail"]) {
      update_user($_SESSION["user"]["data"]["username"], $_POST["nome"], $_POST["cognome"], $_POST["mail"]);
      $_SESSION["user"]["data"] = get_user($_SESSION["user"]["data"]["username"]);
      _set_cookie($_SESSION["user"]);
    } else {
      echo "<script>alert('Per favore, compila tutti i campi')</script>";
    }
  }

  if (isset($_POST["new_password"]) && isset($_POST["repeat_new_password"])) {
    if ($_POST["new_password"] && $_POST["repeat_new_password"]) {
      if ($_POST["new_password"] == $_POST["repeat_new_password"]) {
        update_password($_SESSION["user"]["data"]["username"], $_POST["new_password"]);
      } else {
        echo "<script>alert('Le password non coincidono.')</script>";
      }
    } else {
      echo "<script>alert('Per favore, compila tutti i campi')</script>";
    }
  }

  if (isset($_POST["change_preferences"])) {
    if ($_POST["change_preferences"]) {
      if ($_SESSION["user"]["preference"]) {
        update_preference($_SESSION["user"]["data"]["username"], $_POST["change_preferences"]);
      } else {
        new_preference($_SESSION["user"]["data"]["username"], $_POST["change_preferences"]);
      }
    }
    $_SESSION["user"]["preference"] = get_preference($_SESSION["user"]["data"]["username"]);
    _set_cookie($_SESSION["user"]);
  }

  if (isset($_POST["new_user_username"]) && isset($_POST["new_user_nome"]) && isset($_POST["new_user_cognome"]) && isset($_POST["new_user_mail"]) && isset($_POST["new_user_password"])) {
    if ($_POST["new_user_username"] && $_POST["new_user_nome"] && $_POST["new_user_cognome"] && $_POST["new_user_mail"] && $_POST["new_user_password"]) {
      insert_user($_POST["new_user_username"], $_POST["new_user_nome"], $_POST["new_user_cognome"], $_POST["new_user_mail"], $_POST["new_user_password"]);
    } else {
      echo "<script>alert('Per favore, compila tutti i campi')</script>";
    }
  }

  if (isset($_POST["delete_user"])) {
    if ($_POST["delete_user"]) {
      delete_user($_POST["delete_user"]);
    }
  }

if (isset($_POST["new_content_titolo"]) && isset($_POST["new_content_genere"]) && isset($_POST["new_content_autore"]) && isset($_POST["new_content_album"]) && isset($_POST["new_content_playlist"])) {
  if ($_POST["new_content_titolo"] && $_POST["new_content_autore"] && $_FILES["new_content_filename"]["name"] && $_FILES["new_content_copertina"]["name"]) {
    $audio_dir = "assets/music/";
    $copertina_dir = "assets/posts/";
    $audio_file = $audio_dir . basename($_FILES["new_content_filename"]["name"]);
    $copertina_file = $copertina_dir . basename($_FILES["new_content_copertina"]["name"]);
    $uploadOk = 1;

    if (move_uploaded_file($_FILES["new_content_filename"]["tmp_name"], $audio_file) && move_uploaded_file($_FILES["new_content_copertina"]["tmp_name"], $copertina_file)) {
        $uploadOk = 1;
    } else {
        $uploadOk = 0;
    }
    new_contenuto($_POST["new_content_genere"], $_POST["new_content_titolo"], $_POST["new_content_autore"], $_POST["new_content_playlist"], $_POST["new_content_album"], "assets/music/" . basename($_FILES["new_content_filename"]["name"]), "assets/posts/" . basename($_FILES["new_content_copertina"]["name"]));
  } else {
    echo "<script>alert('Per favore, compila tutti i campi')</script>";
  }
}

if (isset($_POST["delete_content"])) {
  if ($_POST["delete_content"]) {
    delete_contenuto($_POST["delete_content"]);
  }
}

?>
<section class = "container">
  <div class="content">
    <section class="centered">
      <h1>Il mio utente</h1>
      <h3>Cambia le informazioni di login</h3>
      <form class="" action="settings.php" method="post">
        <input type="text" class="text_field" name="nome" value="<?=$_SESSION["user"]["data"]["nome"] ?>" placeholder="Nome">
        <input type="text" class="text_field" name="cognome" value="<?=$_SESSION["user"]["data"]["cognome"] ?>" placeholder="Cognome">
        <input type="text" class="text_field" name="mail" value="<?=$_SESSION["user"]["data"]["mail"] ?>" placeholder="Mail">
        <input class="button" type="submit" name="" value="Salva">
      </form>

      <h3>Cambia la password</h3>
      <form class="" action="settings.php" method="post">
        <input type="password" class="text_field" name="new_password" value="" placeholder="Nuova password">
        <input type="password" class="text_field" name="repeat_new_password" value="" placeholder="Ripeti nuova">
        <input class="button" type="submit" name="" value="Salva">
      </form>

      <h3>Cambia preferenze musicali</h3>
      <form class="" action="settings.php" method="post">
        <select class="text_field" name="change_preferences">
          <?php if ($_SESSION["user"]["preference"]){ ?>
            <option value="<?=$_SESSION["user"]["preference"] ?>"><?=get_genere_by_key($_SESSION["user"]["preference"])["descrizione"] ?></option>
          <?php } else { ?>
            <option value="">-- Seleziona un genere --</option>
          <?php } ?>
          <?php foreach (get_generi_not_equal_to($_SESSION["user"]["preference"]) as $key){ ?>
            <option value="<?=$key[0] ?>"><?=$key[1] ?></option>
          <?php } ?>
        </select>
        <input class="button" type="submit" name="" value="Salva">
      </form>
    </section>

    <!-- ONLY ADMIN -->
    <?php if ($_SESSION["user"]["data"]["user_type_key"] == "admin"){ ?>
      <section class="centered">
        <h1>Strumenti di amministrazione</h1>
        <h2>Gestione utenti</h2>
        <h3>Inserisci utente</h3>
        <form class="" action="settings.php" method="post">
          <input type="text" class="text_field" name="new_user_username" value="" placeholder="Username">
          <input type="text" class="text_field" name="new_user_nome" value="" placeholder="Nome">
          <input type="text" class="text_field" name="new_user_cognome" value="" placeholder="Cognome">
          <input type="text" class="text_field" name="new_user_mail" value="" placeholder="Mail">
          <input type="password" class="text_field" name="new_user_password" value="" placeholder="Password">
          <input class="button" type="submit" name="" value="Salva">
        </form>
        <h3>Tutti gli utenti</h3>
        <table class="palinsesto_table">
          <tr>
            <th>Username</th>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Mail</th>
            <th>Tipo utente</th>
            <th>Modifica</th>
            <th>Elimina</th>
          </tr>
          <?php foreach (get_all_users() as $all_users){ ?>
            <tr>
              <th><?= $all_users[0]?></th>
              <th><?= $all_users[2]?></th>
              <th><?= $all_users[3]?></th>
              <th><?= $all_users[4]?></th>
              <th><?= $all_users[1]?></th>
              <th>
                <form action="edit_user.php" method="post">
                  <input type="hidden" name="edit_user" value="<?=$all_users[0] ?>">
                  <input class="button" type="submit" name="" value="Edit">
                </form>
              </th>
              <th>
                <form action="settings.php" method="post">
                  <input type="hidden" name="delete_user" value="<?=$all_users[0] ?>">
                  <input class="button" type="submit" name="" value="Delete">
                </form>
              </th>
            </tr>
          <?php } ?>
        </table>
      </section>
      <section class="centered">
        <h2>Gestione contenuti</h2>
        <h3>inserisci contenuto</h3>
        <form class="" action="settings.php" method="post" enctype="multipart/form-data">
          <input type="text" class="text_field" name="new_content_titolo" value="" placeholder="Titolo*">
          <input type="text" class="text_field" name="new_content_autore" value="" placeholder="Autore*">
          <input type="text" class="text_field" name="new_content_album" value="" placeholder="Album">
          <select class="text_field" name="new_content_genere">
            <?php foreach (get_generi() as $key){ ?>
              <option value="<?=$key[0] ?>"><?=$key[1] ?></option>
            <?php } ?>
          </select>
          <input type="text" class="text_field" name="new_content_playlist" value="" placeholder="Playlist">
          <br>
          <label for="new_content_copertina">Copertina*:</label>
          <input type="file" class="text_field" name="new_content_copertina">
          <label for="new_content_filename">File audio*:</label>
          <input type="file" class="text_field" name="new_content_filename">
          <br>
          <input class="button" type="submit" name="" value="Salva">
        </form>
        <h3>Elimina contenuti</h3>
        <table class="palinsesto_table">
          <tr>
            <th>Titolo</th>
            <th>Autore</th>
            <th>Album</th>
            <th>Playlist</th>
            <th>Genere</th>
            <th>Elimina</th>
          </tr>
          <?php foreach (get_contenuti() as $contenuto){ ?>
            <tr>
              <th><?= $contenuto[2]?></th>
              <th><?= $contenuto[3]?></th>
              <th><?= $contenuto[5]?></th>
              <th><?= $contenuto[4]?></th>
              <th><?= $contenuto[1]?></th>
              <th>
                <form action="settings.php" method="post">
                  <input type="hidden" name="delete_content" value="<?=$contenuto[0] ?>">
                  <input class="button" type="submit" name="" value="Delete">
                </form>
              </th>
            </tr>
          <?php } ?>
        </table>
      </section>
    <?php } ?>

  </div>
</section>
<?php include_once 'components/footer.php'; ?>
