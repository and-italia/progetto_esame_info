<?php
  function get_contenuti()
  {
    $conn = db_connect();

    $sql = "SELECT * FROM contenuti";
    $res = mysqli_query($conn, $sql);
    $ret = mysqli_fetch_all($res);

    mysqli_close($conn);

    return $ret;
  }

  function get_contenuti_by_id($id_contento)
  {
    $conn = db_connect();

    $sql = "SELECT * FROM contenuti WHERE id_contenuto = '$id_contento'";
    $res = mysqli_query($conn, $sql);
    $ret = mysqli_fetch_assoc($res);

    mysqli_close($conn);

    return $ret;
  }

  function search_contenuti($string)
  {
    $conn = db_connect();

    $sql = "SELECT *
            FROM contenuti
            WHERE key_genere LIKE '%$string%'
            OR titolo LIKE '%$string%'
            OR autore LIKE '%$string%'
            OR playlist LIKE '%$string%'
            OR album LIKE '%$string%';";

    $res = mysqli_query($conn, $sql);
    $ret = mysqli_fetch_all($res);

    mysqli_close($conn);

    return $ret;
  }

  function get_contenuti_by_genere($key_genere)
  {
    $conn = db_connect();

    $sql = "SELECT * FROM contenuti WHERE key_genere = '$key_genere'";
    $res = mysqli_query($conn, $sql);
    $ret = mysqli_fetch_all($res);

    mysqli_close($conn);
    return $ret;
  }

  function get_contenuti_by_playlist($playlist_name)
  {
    $conn = db_connect();

    $sql = "SELECT * FROM contenuti WHERE playlist = '$playlist_name'";
    $res = mysqli_query($conn, $sql);
    $ret = mysqli_fetch_all($res);

    mysqli_close($conn);
    return $ret;
  }

  function new_contenuto($key_genere, $titolo, $autore, $playlist, $album, $filename, $copertina)
  {
    $conn = db_connect();

    $sql = "INSERT INTO contenuti (key_genere, titolo, autore, playlist, album, filename, copertina) VALUES ('$key_genere', '$titolo', '$autore', '$playlist', '$album', '$filename', '$copertina')";

    mysqli_query($conn, $sql);

    mysqli_close($conn);
  }

  function update_contenuto($id_contenuto, $key_genere, $titolo, $autore, $playlist, $album, $filename)
  {
    $conn = db_connect();
    $sql = "UPDATE contenuti SET key_genere = '$key_genere', titolo = '$titolo', autore = '$autore', playlist = '$playlist', album = '$album', filename = '$filename' WHERE id_contenuto = $id_contenuto";
    mysqli_query($conn, $sql);
    mysqli_close($conn);
  }

  function delete_contenuto($id_contenuto)
  {
    $conn = db_connect();
    $sql = "DELETE FROM contenuti WHERE id_contenuto = $id_contenuto";
    mysqli_query($conn, $sql);
    mysqli_close($conn);
  }

?>
