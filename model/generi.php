<?php
  function get_generi()
  {
    $conn = db_connect();

    $sql = "SELECT * FROM generi";
    $res = mysqli_query($conn, $sql);
    $ret = mysqli_fetch_all($res);

    mysqli_close($conn);
    return $ret;
  }

  function get_generi_not_equal_to($key)
  {
    $conn = db_connect();

    $sql = "SELECT * FROM generi WHERE key_generi <> '$key'";
    $res = mysqli_query($conn, $sql);
    $ret = mysqli_fetch_all($res);

    mysqli_close($conn);
    return $ret;
  }

  function get_genere_by_key($key)
  {
    $conn = db_connect();

    $sql = "SELECT * FROM generi WHERE key_generi = '$key'";
    $res = mysqli_query($conn, $sql);
    $ret = mysqli_fetch_assoc($res);

    mysqli_close($conn);
    return $ret;
  }

  function new_genere($key_genere, $descrizione)
  {
    $conn = db_connect();

    $sql = "INSERT INTO generi (key_generi, descrizione) VALUES ('$key_genere', '$descrizione')";

    mysqli_query($conn, $sql);
    mysqli_close($conn);
  }


  function delete_genere($username)
  {
    $conn = db_connect();
    $sql = "DELETE FROM generi WHERE username='$username'";
    mysqli_query($conn, $sql);
    mysqli_close($conn);
  }

?>
