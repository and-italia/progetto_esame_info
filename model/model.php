<?php
  function db_connect ()
  {
    $db_host = "localhost";
    $db_user = "root";
    $db_password = "";
    $db_name = "progetto_esame";

    $conn = mysqli_connect($db_host, $db_user, $db_password, $db_name) or
    die("Failed to connect to MySQL: " . mysqli_connect_error());

    return $conn;
  }
?>
