<?php
  function get_user_types()
  {
    $conn = db_connect();

    $sql = "SELECT * FROM user_type";
    $res = mysqli_query($conn, $sql);
    $ret = mysqli_fetch_all($res);

    mysqli_close($conn);
    return $ret;
  }

  function get_user_type($key)
  {
    $conn = db_connect();

    $sql = "SELECT * FROM user_type WHERE user_type_key = '$key'";
    $res = mysqli_query($conn, $sql);
    $ret = mysqli_fetch_assoc($res);

    mysqli_close($conn);
    return $ret;
  }
?>
