<?php
  function login($username, $password)
  {
    $conn = db_connect();
    $password = encrypt($password);
    $sql = "SELECT * FROM utenti WHERE username = '$username' AND password = '$password'";
    $res = mysqli_query($conn, $sql);

    $count = mysqli_num_rows($res);
    // $res = mysqli_fetch_assoc($res);
    if ($count > 0) {
      $res = true;
    } else {
      $res = false;
    }
    mysqli_close($conn);

    return $res;
  }

  function get_all_users()
  {
    $conn = db_connect();
    $sql = "SELECT * FROM utenti";
    $res = mysqli_query($conn, $sql);
    $ret = mysqli_fetch_all($res);

    mysqli_close($conn);

    return $ret;
  }

  function get_user($username)
  {
    $conn = db_connect();
    $sql = "SELECT * FROM utenti WHERE username = '$username'";
    $res = mysqli_query($conn, $sql);
    $ret = mysqli_fetch_assoc($res);

    mysqli_close($conn);

    return $ret;
  }

  function insert_user($username, $nome, $cognome, $mail, $password)
  {
    $conn = db_connect();
    $password = encrypt($password);
    $sql = "INSERT INTO utenti (username, user_type_key, nome, cognome, mail, password) VALUES ('$username', 'user', '$nome', '$cognome', '$mail', '$password')";
    mysqli_query($conn, $sql);

    mysqli_close($conn);
  }

  function delete_user($username)
  {
    $conn = db_connect();
    $sql = "DELETE FROM preferenze WHERE username = '$username'";
    mysqli_query($conn, $sql);
    $sql = "DELETE FROM utenti WHERE username = '$username'";
    mysqli_query($conn, $sql);

    mysqli_close($conn);
  }

  function update_user($username, $nome, $cognome, $mail)
  {
    $conn = db_connect();

    $sql = "UPDATE utenti SET nome = '$nome', cognome = '$cognome', mail = '$mail' WHERE username = '$username'";
    mysqli_query($conn, $sql);

    mysqli_close($conn);
  }

  function update_user_type($username, $user_type)
  {
    $conn = db_connect();

    $sql = "UPDATE utenti SET user_type_key = '$user_type' WHERE username = '$username'";
    mysqli_query($conn, $sql);

    mysqli_close($conn);
  }

  function update_password($username, $password)
  {
    $conn = db_connect();
    $password = encrypt($password);
    $sql = "UPDATE utenti SET password = '$password' WHERE username = '$username'";
    mysqli_query($conn, $sql);

    mysqli_close($conn);
  }

  function is_username_free($username)
  {
    $conn = db_connect();
    $sql = "SELECT * FROM utenti WHERE username = '$username'";
    $res = mysqli_query($conn, $sql);

    $count = mysqli_num_rows($res);
    // $res = mysqli_fetch_assoc($res);
    if ($count > 0) {
      $res = false;
    } else {
      $res = true;
    }
    mysqli_close($conn);

    return $res;
  }

  function encrypt($password)
  {
    return hash('sha256', $password);
  }
?>
