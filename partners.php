<?php include_once 'components/header.php'; ?>
    <section class = "container">
      <div class="content">
        <h1>Partners</h1>
        <a href="https://www.unito.it" class="logo_partner">
          <img src="https://www.unito.it/sites/all/themes/unitowww/img/unitoit.svg"  alt="Università di Torino" title="Università di Torino">
        </a>
        <a href="https://www.and-italia.it" class="logo_partner">
          <img src="https://and-italia.it/wp-content/uploads/2018/03/logo_and_nuovo.png"  alt="And Italia" title="And Italia">
        </a>
      </div>
    </section>
<?php include_once 'components/footer.php'; ?>
