<?php
  include_once 'components/header.php';
  require_admin();

  include_once 'model/model.php';
  include_once 'model/generi.php';
  include_once 'model/utenti.php';
  include_once 'model/preferenze.php';
  include_once 'model/user_type.php';

  $err_login_info = "";
  $err_password = "";

  if (isset($_POST["edit_user"])) {
    $_SESSION["edit_user_username_passed"] = $_POST["edit_user"];
  }

  $selected_user = get_user($_SESSION["edit_user_username_passed"]);
  $selected_user_preferences = get_preference($selected_user["username"]);

  if (isset($_POST["nome"]) && isset($_POST["cognome"]) && isset($_POST["mail"])) {
    if ($_POST["nome"] && $_POST["cognome"] && $_POST["mail"]) {
      update_user($selected_user["username"], $_POST["nome"], $_POST["cognome"], $_POST["mail"]);
      header("Location: settings.php");
      die();
    } else {
      echo "<script>alert('Per favore, compila tutti i campi')</script>";
    }
  }

  if (isset($_POST["new_password"]) && isset($_POST["repeat_new_password"])) {
    if ($_POST["new_password"] && $_POST["repeat_new_password"]) {
      if ($_POST["new_password"] == $_POST["repeat_new_password"]) {
        update_password($selected_user["username"], $_POST["new_password"]);
        header("Location: settings.php");
        die();
      } else {
        echo "<script>alert('Le password non coincidono')</script>";
      }
    } else {
      echo "<script>alert('Per favore, compila tutti i campi')</script>";
    }
  }

  if (isset($_POST["change_preferences"])) {
    if ($_POST["change_preferences"]) {
      if ($selected_user_preferences) {
        update_preference($selected_user["username"], $_POST["change_preferences"]);
      } else {
        new_preference($selected_user["username"], $_POST["change_preferences"]);
      }
      header("Location: settings.php");
      die();
    }
  }

  if (isset($_POST["change_user_type"])) {
    if ($_POST["change_user_type"]) {
      update_user_type($selected_user["username"], $_POST["change_user_type"]);
      header("Location: settings.php");
      die();
    }
  }

?>
<section class = "container">
  <div class="content">
    <section class="centered">
      <h1>Modifica utente "<?=$_SESSION["edit_user_username_passed"] ?>"</h1>
      <h3>Cambia le informazioni di login</h3>
      <form class="" action="edit_user.php" method="post">
        <input class="text_field" type="text" name="nome" value="<?=$selected_user["nome"] ?>" placeholder="Nome">
        <input class="text_field" type="text" name="cognome" value="<?=$selected_user["cognome"] ?>" placeholder="Cognome">
        <input class="text_field" type="text" name="mail" value="<?=$selected_user["mail"] ?>" placeholder="Mail">
        <input class="button" type="submit" name="" value="Salva">
      </form>

      <h3>Cambia la password</h3>
      <form class="" action="edit_user.php" method="post">
        <input class="text_field" type="password" name="new_password" value="" placeholder="Nuova password">
        <input class="text_field" type="password" name="repeat_new_password" value="" placeholder="Ripeti nuova">
        <input class="button" type="submit" name="" value="Salva">
      </form>

      <h3>Cambia preferenze musicali</h3>
      <form class="" action="edit_user.php" method="post">
        <select class="text_field" name="change_preferences">
          <?php if ($selected_user_preferences){ ?>
            <option value="<?=$selected_user_preferences ?>"><?=get_genere_by_key($selected_user_preferences)["descrizione"] ?></option>
          <?php } else { ?>
            <option value="">-- Seleziona un genere --</option>
          <?php } ?>
          <?php foreach (get_generi_not_equal_to($selected_user_preferences) as $key){ ?>
            <option value="<?=$key[0] ?>"><?=$key[1] ?></option>
          <?php } ?>
        </select>
        <input class="button" type="submit" name="" value="Salva">
      </form>

      <h3>Cambia tipo di utenza</h3>
      <form class="" action="edit_user.php" method="post">
        <select class="text_field" name="change_user_type">
          <option value=""><?=get_user_type($selected_user["user_type_key"])["descrizione"] ?></option>
          <?php foreach (get_user_types() as $key){ ?>
            <?php if ($selected_user["user_type_key"] != $key[0]){ ?>
              <option value="<?=$key[0] ?>"><?=$key[1] ?></option>
            <?php } ?>
          <?php } ?>
        </select>
        <input class="button" type="submit" name="" value="Salva">
      </form>
    </section>
  </div>
</section>
<?php include_once 'components/footer.php'; ?>
