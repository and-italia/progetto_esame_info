<?php include_once 'components/header.php'; ?>
<section class = "container">
  <div class="content">
    <h1>Palinsesto</h1>

    <section class="form_container">
      <h3 class="centered">Qui trovi gli orari in cui trasmettiamo</h3>
      <table class="palinsesto_table">
        <tr>
          <th>Giorno</th>
          <th>Programma</th>
          <th>Ora</th>
        </tr>
        <tr>
          <td>Lunedì</td>
          <td>Rock classics</td>
          <td>9:00 - 18</td>
        </tr>
        <tr>
          <td>Mercoledì</td>
          <td>Indie italia</td>
          <td>9:00 - 18:00</td>
        </tr>
        <tr>
          <td>Venerdì</td>
          <td>Uniparty</td>
          <td>18.30 - 2:00</td>
        </tr>
      </table>
    </section>
  </div>
</section>
<?php include_once 'components/footer.php'; ?>
